const mongoose = require('mongoose');

const employeeSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "Firstname is required"]
    },
    lastName: {
        type: String,
        required: [true, "Lastname is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    age: {
        type: Number,
        required: [true, "Age is required"]
    },
    birthDate: {
      type: String,
      required: [true, "BirthDate is required"]
    },
    password: {
        type: String,
        required: [true, "Passwordis required"]
    }
})

module.exports = mongoose.model("EmployeeList", employeeSchema);