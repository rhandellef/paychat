const Employee = require('../models/Employee');
const bcrypt = require('bcryptjs');
const auth = require('../auth');
// const { reset } = require('nodemon');

// CREATE PRODUCT ADMIN ONLY
module.exports.addEmployee = (req, res) => {
    console.log(req.body)
    let newEmployee = new Employee({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        age: req.body.age,
        birthDate: req.body.birthDate,
        password: req.body.password
     })

            newEmployee.save()
                .then(result => res.send(result))
                .catch(err => res.send(err))
        };

// GET ALL PRODUCTS
module.exports.getAllEmployees = (req, res) => {
    Employee.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
};


// SEARCH EMPLOYEE
module.exports.search = (req, res) => {
    console.log(req.body)
    Employee.find({name : {$regex : req.body.firstName ,$options : '$i'}})

    .then(result => {
        if(result.length === 0){
            return res.send('Employee is not found')
        } else {
            return res.send(result)
        }
    })
}

// Sorting Products in Ascending Order
module.exports.sortAscending = (req, res) => {
    Employee.find({})
        .sort({name : 1})
        .then(result => res.send(result))
        .catch(err => res.send(err))
}

// Sorting Products in Descending Order

module.exports.sortDescending = (req, res) => {
    Employee.find({})
        .sort({name : -1})
        .then(result => res.send(result))
        .catch(err => res.send(err))
}


module.exports.category = (req, res) => {
    console.log(req.body)
    Employee.find({category : {$regex : req.body.email ,$options : '$i'}})
        .then(result => res.send(result))
        .catch(err => res.send(err))
}

// module.exports.searchByCategory = (req, res) => {
//     console.log(req.body)
//     Product.find({category : {$regex : req.body.category ,$options : '$i'}})

//     .then(result => {
//         if(result.length === 0){
//             return res.send('Product is not available at the moment')
//         } else {
//             return res.send(result)
//         }
//     })
// }


// DEACTIVATE PRODUCT
module.exports.deactivateEmployee = (req, res) => {
    console.log(req.body)
    let updates = {
        quantity: false
    }
    Employee.findByIdAndUpdate(req.params.id,updates, {new: true} )
        .then(result => res.send(result))
        .catch(err => res.send(err))
}

//! Purchase History
module.exports.purchaseHistory = (req, res) => {
    console.log(req.body)
    Employee.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
}


// !View Specific Product
module.exports.viewEmployees = (req, res) => {
    console.log(req.body)
    Employee.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
}

// Activate Product
module.exports.activateEmployee = (req, res) => {
    console.log(req.body)
    let updates = {
        quantity: true
    }
    Employee.findByIdAndUpdate(req.params.id,updates, {new: true} )
        .then(result => res.send(result))
        .catch(err => res.send(err))
}

// UPDATE PRODUCT
module.exports.updateEmployee = (req, res) => {
    let updates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        age: req.body.age,
        birthDate: req.body.birthDate,
        password: req.body.password
    }
    Employee.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedEmployee => res.send(updatedEmployee))
    .catch(err => res.send(err));
};