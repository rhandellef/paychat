//! [SECTION DEPENDENCIES]
const express = require('express');
const router = express.Router();
const auth = require('../auth');
const { verify, verifyAdmin } = auth;

// ! CONTROLLER DEPENDENCIES
const employeeControllers = require('../controllers/employeeControllers');




// CREATE PRODUCT ADMIN ONLY
router.post('/add', verify, verifyAdmin, employeeControllers.addEmployee);

// GET ALL PRODUCTS
router.get('/getAllEmployees', employeeControllers.getAllEmployees);

// SEARCH PRODUCTS
router.post('/search', employeeControllers.search);
// SORT ASCENDING
router.get('/sortAscending', employeeControllers.sortAscending);
// SORT DESCENDING
router.get('/sortDescending', employeeControllers.sortDescending);

// GET PRODUCTS BY CATEGORY
router.post('/category', employeeControllers.category);

// Deactivate Product
router.put('/deactivateEmployee/:id',  employeeControllers.deactivateEmployee);

// View Specific Product
router.get('/viewEmployees/:id', employeeControllers.viewEmployees);

// Activate Product
router.put('/activateEmployee/:id',  employeeControllers.activateEmployee);

// Update Product
router.put('/updateEmployee/:id', verify, verifyAdmin, employeeControllers.updateEmployee);

module.exports = router;
